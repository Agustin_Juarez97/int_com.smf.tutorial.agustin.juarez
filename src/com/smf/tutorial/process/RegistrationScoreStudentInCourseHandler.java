package com.smf.tutorial.process;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.plm.Product;

import com.smf.tutorial.data.CourseStudent;
import com.smf.tutorial.data.EditionCourse;;

public class RegistrationScoreStudentInCourseHandler extends BaseProcessActionHandler {

	@Override
	protected JSONObject doExecute(Map<String, Object> parameters, String content) {
		JSONObject result = new JSONObject();
		OBContext.setAdminMode(true); 
		
		try {
			JSONObject request = new JSONObject(content);
			JSONObject params = request.getJSONObject("_params");
			
			//Obtengo fecha actual
			Date dateCurrent = new Date();
			
			//Obtengo el ID del curso,ID del alumno y su puntaje
			String idCourse = params.getString("m_product_id");
			String idBusinessPartner = params.getString("c_bpartner_id");
			String score = params.getString("score"); 
			
			Product courseStudent = getCourse(idCourse);									//Obtengo el curso correspondiente al ID idCourse
			BusinessPartner bp = getStudent(idBusinessPartner);								//Obtendo el estudiante correspondiete al ID idBusinessPartner 
			List<CourseStudent> listCourseStudent = getCourseStudent(courseStudent, bp);	//Obtener todas las inscripciones del alumno en el curso determinado
			
			if (listCourseStudent.isEmpty()) {
				result.put("message", getErrorMessage("The student is not enrolled in any edition of the course"));
			} else {
				CourseStudent c = getCurrentRegistration(listCourseStudent, dateCurrent);
				if (c != null) {
					result.put("message", getSuccessMessage());
					c.setScore(score);
				} else {
					result.put("retryExecution", true);
					result.put("message", getErrorMessage("There is no current registration"));
				}
			} 
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * Metodo privado el cual me permite a partir de un ID de curso obtener el curso completo
	 * @param idCourse, ID del curso a buscar
	 * @return Product, curso completo
	 */
	private Product getCourse(String idCourse) {
		return OBDal.getInstance().get(Product.class,idCourse);
	}
	
	/**
	 * Metodo privado el cual me permite a partir de un ID de un estudiante obtener el estuidante completo
	 * @param idBusinessPartner, ID del estudiante a buscar
	 * @return BusinessPartner, estudiante completo
	 */
	private BusinessPartner getStudent(String idBusinessPartner) {	
		return OBDal.getInstance().get(BusinessPartner.class,idBusinessPartner);
	}
	
	/**
	 * Metodo privado el cual me permite obtener todas las inscripciones de un alumno en un curso particular
	 * @param courseStudent
	 * @param bp
	 * @return List<CourseStudent>, lista de todas las inscripciones al curso
	 */
	private List<CourseStudent> getCourseStudent(Product courseStudent, BusinessPartner bp) {
		OBCriteria<CourseStudent> cs = OBDal.getInstance().createCriteria(CourseStudent.class);
		cs.add(Restrictions.eq(CourseStudent.PROPERTY_BUSINESSPARTNER, bp));
		cs.add(Restrictions.in(CourseStudent.PROPERTY_SMFTEDITIONCOURSE, getEdtionCourse(courseStudent)));
		
		return cs.list();
	}
	
	/**
	 * Metodo privado el cual me permite buscar la insripcion activa
	 * @param CourseStudent
	 * @param date
	 * @return CourseStudent, inscripcion activa
	 */
	private CourseStudent getCurrentRegistration (List<CourseStudent> courseStudent, Date date) {
		for (CourseStudent cs : courseStudent) {
			if (date.compareTo(cs.getDateEndCourse()) < 0)	//obtengo el que tenga la fecha menor a la fecha actual (no vencido)
				return cs;
		}
		return null;
	}
	
	/**
	 * Metodo el cual me permite obtener todas las ediciones de un curso
	 * @param course, curso del cual quiero saber sus ediciones
	 * @return List<EditionCourse>
	 */
	private List<EditionCourse> getEdtionCourse(Product course) {
		OBCriteria<EditionCourse> editionCourse = OBDal.getInstance().createCriteria(EditionCourse.class);
		editionCourse.add(Restrictions.eq(EditionCourse.PROPERTY_PRODUCT, course));
		List<EditionCourse> result = editionCourse.list();
		
		return result;
	}
	
	/**
	 * Metodo el cual me permite retornar un mensaje de exito cuando se puedo insertar el puntaje del alumno al curso
	 * @return JSONObject, el cual contiene el mensaje de exito de la operacion
	 * @throws JSONException
	 */
	private JSONObject getSuccessMessage() throws JSONException {
		JSONObject msg = new JSONObject();
		msg.put("severity", "success");
		msg.put("text", "Score recorded in student enrollment");
		return msg;
	}
	
	/**
	 * Metodo el cual me permite retornar un menajes de error cuando no se insertar el puntaje del alumno al curso
	 * @param  txtMsg, mensaje de error a mostrar
	 * @return JSONObject, el cual contiene el mensaje de error de la operacion
	 * @throws JSONException
	 */
	private JSONObject getErrorMessage(String txtMsg) throws JSONException {
		JSONObject msg = new JSONObject();
		msg.put("severity", "error");
		msg.put("text", txtMsg);
		return msg;
	}
}
