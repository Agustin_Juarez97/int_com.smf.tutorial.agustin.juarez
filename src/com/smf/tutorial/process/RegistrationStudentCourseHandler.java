package com.smf.tutorial.process;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.businesspartner.BusinessPartner;


import com.smf.tutorial.data.CourseStudent;
import com.smf.tutorial.data.EditionCourse;

public class RegistrationStudentCourseHandler extends BaseProcessActionHandler {

	@Override
	protected JSONObject doExecute(Map<String, Object> parameters, String content) {
		JSONObject result = new JSONObject();
		OBContext.setAdminMode(true);
		
		try {
			JSONObject request = new JSONObject(content);
			JSONObject params = request.getJSONObject("_params");
			
			//Obtengo ID del alumno y de la edicion del curso
			String idBusinessPartner = params.getString("c_bpartner_id");
			String idEditionCourse = params.getString("smft_edition_course_id");
			
			//Obtengo el alumno correspondiente al ID idBusinessPartner
			OBCriteria<BusinessPartner> bp = OBDal.getInstance().createCriteria(BusinessPartner.class);
			bp.add(Restrictions.eq(BusinessPartner.PROPERTY_ID, idBusinessPartner));
			List<BusinessPartner> listBP = bp.list();
			
			BusinessPartner businessPartner = listBP.get(0);
			
			//Obtengo el curso correspondient al ID idEditionCourse
			OBCriteria<EditionCourse> ec = OBDal.getInstance().createCriteria(EditionCourse.class);
			ec.add(Restrictions.eq(EditionCourse.PROPERTY_ID, idEditionCourse));
			List<EditionCourse> listEC = ec.list();
			
			EditionCourse editionCourse = listEC.get(0);
			
			//Obtengo la fecha de inicio del curso al que se inscribe
			String date = params.getString("date_begin_course");
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd"); 
			Date newDateBeginCourse = format.parse(date);
			/*----------------------------------------------------------------*/
		
			//Creo nuevo objeto CourseStudent
			CourseStudent coursestudent = OBProvider.getInstance().get(CourseStudent.class);
			coursestudent.setNewOBObject(true);
			coursestudent.setBusinessPartner(businessPartner);
			coursestudent.setSmftEditionCourse(editionCourse);
			coursestudent.setDateBeginCourse(newDateBeginCourse);
			//Guardo el objeto
			OBDal.getInstance().save(coursestudent);
			OBDal.getInstance().flush();
			
			result.put("message", getSuccessMessage());
			return result;
		} catch (Exception e) {
			OBDal.getInstance().rollbackAndClose();
			try {
				result.put("retryExecution", true);
				result.put("message", getErrorMessage(e.getMessage()));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} 
		return result;
	}
	
	/**
	 * Metodo el cual me permite retornar un mensaje de exito cuando se puede inscribir un alumno a un curso
	 * @return JSONObject, el cual contiene el mensaje de exito de la operacion
	 * @throws JSONException
	 */
	private JSONObject getSuccessMessage() throws JSONException {
		JSONObject msg = new JSONObject();
		msg.put("severity", "success");
		msg.put("text", "Student registered to the course");
		return msg;
	}
	
	/**
	 * Metodo el cual me permite retornar un menajes de error cuando no se puede inscribir un alumno a un curso
	 * @param  txtMsg, mensaje de error a mostrar
	 * @return JSONObject, el cual contiene el mensaje de error de la operacion
	 * @throws JSONException
	 */
	private JSONObject getErrorMessage(String txtMsg) throws JSONException {
		JSONObject msg = new JSONObject();
		msg.put("severity", "error");
		msg.put("text", txtMsg);
		return msg;
	}
}
