package com.smf.tutorial.process;

import org.openbravo.dal.service.OBDal;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.service.db.DalBaseProcess;

import java.util.Date;
import java.util.List;
import java.util.Calendar;

import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import com.smf.tutorial.data.CourseStudent;

public class UpdateRegistrationProcess extends DalBaseProcess {

	@Override
	public void doExecute(ProcessBundle bundle) throws Exception {
		//Obtengo fecha actual
		Date dateCurrent = new Date();
		
		//Obtengo fecha del dia anterior
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dateCurrent);
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		Date date = calendar.getTime();
		
		//Obtengo todas las incripciones de cursos las cuales vencieron el dia anterior
		OBCriteria<CourseStudent> courseStudent = OBDal.getInstance().createCriteria(CourseStudent.class);
		courseStudent.add(Restrictions.eq(CourseStudent.PROPERTY_DATEENDCOURSE, date));
		List<CourseStudent> cs = courseStudent.list();
		
		for (CourseStudent c : cs) {
			Calendar calendarCourseStudent = Calendar.getInstance();
			calendarCourseStudent.setTime(c.getDateEndCourse());
			calendarCourseStudent.add(Calendar.MONTH, 1);
			c.setDateEndCourse(calendarCourseStudent.getTime());
		}
	}
}
