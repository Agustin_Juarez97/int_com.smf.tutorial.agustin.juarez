package com.smf.tutorial.eventHandlers;

import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.client.kernel.event.EntityNewEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import com.smf.tutorial.data.CourseStudent;
import com.smf.tutorial.data.EditionCourse;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.enterprise.event.Observes;

import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.model.Entity;

public class StudentEventHandler extends EntityPersistenceEventObserver {
	private static Entity[] entities = {ModelProvider.getInstance().getEntity(CourseStudent.ENTITY_NAME)};
	
	@Override
	protected Entity[] getObservedEntities() {
		return entities;
	}
	
	public void onSave(@Observes EntityNewEvent event) {
		if (!isValidEvent(event)) {
			return;
		}
		
		Entity courseStudentEntity = ModelProvider.getInstance().getEntity(CourseStudent.ENTITY_NAME);
		
		//Obtengo el alumno que se quiere registrar
		Property bpProp = courseStudentEntity.getProperty(CourseStudent.PROPERTY_BUSINESSPARTNER);
		BusinessPartner businessPartner = (BusinessPartner) event.getCurrentState(bpProp);
		
		//Obtengo la edicion de curso a la cual el alumno se quiere registrar, una vez que tengo la edicion de curso obtengo el curso al cual pertenece la misma
		Property editionCourseProp = courseStudentEntity.getProperty(CourseStudent.PROPERTY_SMFTEDITIONCOURSE);
		EditionCourse editionCourse = (EditionCourse) event.getCurrentState(editionCourseProp);
		Product course = (Product) editionCourse.getProduct();
		
		//Obtengo la fecha de inicio del curso al cual el alumno se quiere registrar
		Property dateBeginCourseProp = courseStudentEntity.getProperty(CourseStudent.PROPERTY_DATEBEGINCOURSE);
		Date newDateBeginCourse = (Date) event.getCurrentState(dateBeginCourseProp);
		
		//Una vez que obtengo el curso al cual pertenece la edicion, obtengo la duracion del mismo
		String durationCourse = course.getSmftDurationInMonths();
		
		//Calculo la fecha de finalizacion del curso mediante la fecha de inicio y la duracion del curso
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(newDateBeginCourse);
		calendar.add(Calendar.MONTH, Integer.parseInt(durationCourse));
		Date newDateEndCourse = calendar.getTime();
		
		//Verifico si el alumno ya esta inscripto a ese curso
		boolean registered = isRegistered(businessPartner, course, newDateBeginCourse, newDateEndCourse);
		
		if (!registered) {
			Property courseStudentEndCourseProperty = courseStudentEntity.getProperty(CourseStudent.PROPERTY_DATEENDCOURSE);
			event.setCurrentState(courseStudentEndCourseProperty, newDateEndCourse);
		} else 
			throw new OBException("Registration for the course is active. Wait to finish");
	}
	
	//Metodo privado el cual retirna True si el alumno esta inscripto al curso y ademas el curso esta activo
	//False en caso contrario
	private boolean isRegistered(BusinessPartner businessPartner, Product course, Date dateBeginCourse, Date dateEndCourse) {
		OBCriteria<CourseStudent> courseStudent = OBDal.getInstance().createCriteria(CourseStudent.class);
		courseStudent.add(Restrictions.eq(CourseStudent.PROPERTY_BUSINESSPARTNER, businessPartner));
		courseStudent.add(Restrictions.in(CourseStudent.PROPERTY_SMFTEDITIONCOURSE, getEditions(course)));
		List<CourseStudent> cs = courseStudent.list();
		
		if (!cs.isEmpty()) {
			if (courseActive(cs, dateBeginCourse, dateEndCourse)) //Si el alumno ya esta inscripto verifico si el curso esta activo, es decir todavia lo esta cursando (NO SE VENCIO)
				return true;
			return false;
		}
		else
			return false;
	}
	
	//Metodo privado el cual dado un curso me retorna todas la ediciones del mismo
	private List<EditionCourse> getEditions(Product course) {
		OBCriteria<EditionCourse> editionCourse = OBDal.getInstance().createCriteria(EditionCourse.class);
		editionCourse.add(Restrictions.eq(EditionCourse.PROPERTY_PRODUCT, course));
		List<EditionCourse> result = editionCourse.list();
		
		return result;
	}
	
	//Metodo privado el cual retorna True si de todas las incripciones al curso existe una que este activa, es decir que todavia no haya terminado la fecha de finalizacion
	//False en caso contrario
	private boolean courseActive(List<CourseStudent> cs, Date newDateBeginCourse, Date newDateEndCourse) {
		for (CourseStudent c : cs) {
			if (newDateBeginCourse.compareTo(c.getDateBeginCourse()) < 0) {
				if (newDateEndCourse.compareTo(c.getDateBeginCourse()) > 0) 
					return true;
			}
			if (newDateBeginCourse.compareTo(c.getDateEndCourse()) < 0) {
				if (newDateEndCourse.compareTo(c.getDateEndCourse()) > 0)
					return true;
			}
			if (newDateBeginCourse.compareTo(c.getDateBeginCourse()) > 0) {
				if (newDateEndCourse.compareTo(c.getDateEndCourse()) < 0)
					return true;
			}
			if (newDateBeginCourse.compareTo(c.getDateBeginCourse()) < 0) {
				if (newDateEndCourse.compareTo(c.getDateEndCourse()) > 0)
					return true;
			}
		}
		return false;	
	}
}
